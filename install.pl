#!/usr/bin/env perl

# Written by Francesco Palumbo
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


use strict;
use warnings;

use File::Copy qw(copy);

my $version = 'develop';

die "You must be root to do this!" unless ($ENV{'USER'} eq 'root');

my $man = 'man/bkp.1';
my $prg = 'src/bkp';

my %dest = (
	$man => '/usr/share/man/man1/bkp.1',
	$prg => '/usr/bin/bkp'
);

print "\nYou are installing bkp $version\n";

open(CORE, 'core_modules')
	or die $!;

my @core_modules;

while (my $line = <CORE>) {
    push @core_modules, $line unless $line =~ /^#/;
}
close CORE;


open(EXT, 'external_modules')
	or die $!;

my @external_modules;

while (my $line = <EXT>) {
    push @external_modules, $line unless $line =~ /^#/;
}
close EXT;

my @modules = (@core_modules, @external_modules);
my @not_installed;

my $max_len = 0;
foreach (@modules) {
	my $len = length($_);
	$max_len = $len if ($len > $max_len);
}


sub check_modules {
	my $arrayref = shift;
	
	for (sort @{$arrayref}) {
		chomp;
		print "     Checking for $_";
	
		eval "require $_";
		my $len = length($_);
	
		unless ($@) {
			print ' ' x ($max_len - $len), "[ok]\n";
		}
		else { 
			print ' ' x ($max_len - $len), "[failed]\n";
			push @not_installed, $_;
		}
	}
}



print "\n  Stage 1 : checking for required core modules:\n\n";

check_modules(\@core_modules); 

if (@not_installed) {
	print "\nCannot install bkp, @not_installed module[s] not found!\n";
	exit 1;
}

print "\n  Stage 2 : checking for required external modules:\n\n";

check_modules(\@external_modules);

if (@not_installed) {
	print "Can't find external module[s] @not_installed.\n";
	print "Do you want to install it[them] from CPAN? [y/n]: ";
	
	CONFIRM: {
		my $answer = <STDIN>;
		chomp $answer;

		if ($answer =~ /^y$/) {
			print "Installing @not_installed module[s]:\n";

			if (! system("cpan -i @not_installed")) {
				die "Can't install $_, installation failed\n";
			}
		}
		elsif ($answer =~ /^n$/) {
			die "\nInstallation failed, some ext modules are not installed.\n";
		}
		else {
			print "Please type the correct option 'y' or 'n': ";
			redo CONFIRM;
		}
	}
}

print "\n  Stage 3 : installing program components:\n";

for (keys %dest) {
	print "\n     Installing $_";
	
	copy($_, $dest{$_})
		or die $!;

	chown 0, 0, $dest{$_};
}

chmod 0755, $dest{$prg};

print "\n\nInstallation terminated.\n";
